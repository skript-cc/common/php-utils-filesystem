<?php

declare(strict_types=1);

namespace Skript\Utils\FileSystem;

use PHPUnit\Framework\TestCase;

use function Skript\Utils\Path\join;
use function Skript\Utils\FileSystem\{mkdir, rm, symlink};

final class FunctionsTest extends TestCase
{
    protected static $tmpDir;
    protected $cwd;
    
    public static function setupBeforeClass(): void
    {
        self::$tmpDir = __DIR__.'/.tmp-'.uniqid();
    }
    
    public function setup(): void
    {
        $this->cwd = getcwd();
        \mkdir(self::$tmpDir);
        chdir(self::$tmpDir);
    }
    
    public function tearDown(): void
    {
        rm(self::$tmpDir);
        chdir($this->cwd);
    }
    
    public function testRmRemovesEmptyDirectory(): void
    {
        $dir = join(self::$tmpDir, 'testrm');
        \mkdir($dir);
        $this->assertTrue(rm($dir));
        $this->assertFalse(file_exists($dir));
    }
    
    public function testRmRemovesNonEmptyDirectory(): void
    {
        $dir = join(self::$tmpDir, 'testrm');
        \mkdir($dir);
        touch(join($dir, 'file'));
        $this->assertTrue(rm($dir));
        $this->assertFalse(file_exists($dir));
    }
    
    public function testRmRemovesFile(): void
    {
        $file = join(self::$tmpDir, 'file');
        touch($file);
        $this->assertTrue(rm($file));
        $this->assertFalse(file_exists($file));
    }
    
    public function testMkDirCreatesNewDirectory(): void
    {
        $dir = join(self::$tmpDir, 'new-dir');
        $this->assertTrue(mkdir($dir));
        $this->assertTrue(is_dir($dir));
    }
    
    public function testMkDirCreatesParentDirs(): void
    {
        $dir = join(self::$tmpDir, 'parent-dir', 'new-dir');
        $this->assertTrue(mkdir($dir));
        $this->assertTrue(is_dir($dir));
    }
    
    public function testMkDirDoesNotOverwriteExistingDir(): void
    {
        $dir = join(self::$tmpDir, 'new-dir');
        $file = join($dir, 'file');
        mkdir($dir);
        touch($file);
        
        $this->assertFalse(mkdir($dir));
        $this->assertTrue(is_dir($dir));
        $this->assertTrue(file_exists($file));
    }
    
    public function testMkDirOverwritesExistingDir(): void
    {
        $dir = join(self::$tmpDir, 'new-dir');
        $file = join($dir, 'file');
        mkdir($dir);
        touch($file);
        
        $this->assertTrue(mkdir($dir, true));
        $this->assertTrue(is_dir($dir));
        $this->assertFalse(file_exists($file));
    }
    
    public function testSymlinkWithoutLinkPathCreatesLinkInCwd()
    {
        $targetDir = 'subdir/target_dir';
        $linkLocation = getcwd().'/target_dir';
    
        mkdir($targetDir);
    
        symlink($targetDir);
        
        $this->assertTrue(is_link($linkLocation), 'target_dir should be a symlink');
        $this->assertEquals('subdir/target_dir', readlink($linkLocation), 'symlink should point to target directory');
    }
    
    public function testSymlinkWithLinkPathCreatesLinkInCwd()
    {
        $targetDir = 'subdir/target_dir';
        $linkLocation = getcwd().'/named_link';
    
        mkdir($targetDir);
    
        symlink($targetDir, $linkLocation);
    
        $this->assertTrue(is_link($linkLocation), 'named_dir should be a symlink');
        $this->assertEquals('subdir/target_dir', readlink($linkLocation), 'symlink should point to target directory');
    }
    
    public function testSymlinkThrowsExceptionWhenTargetExists()
    {
        $targetDir = 'target_dir';
        $linkLocation = getcwd();
    
        mkdir($targetDir);
    
        $this->expectException(\Exception::class);
    
        symlink($targetDir);
    }
}