# PHP Utils - FileSystem

FileSystem utilities to make some file operations a little bit easier.

## Functions

- `rm()`: Just removes a path, whether it's a file, link, or (non) empty directory.
- `mkdir()`: Creates a directory without complaining.
- `symlink()`: Resolves all paths to easily create relative symlinks.

## Contributing

Report issues and send pull requests in the [main repository]

[main repository]: https://gitlab.com/skript-cc/common/php-utils